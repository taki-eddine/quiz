<?php

abstract class RESTful {
    private $_httpMethod = null;

    protected $_verb     = null;
    protected $_args     = null;
    protected $_request  = null;
    protected $_response = null;

    ///--------------------------------------
    private function _httpMethodType($args) {
        return (empty($args) ? "Collection" : "Element");
    }

    ///--------------------------------------
    protected function __construct($method, $verb, $args, $request) {
        switch( $method ) {
            case "GET":
            case "POST":
            case "PUT":
            case "DELETE":
                $this->_httpMethod = "do". $method[0] . strtolower(substr($method, 1)) . $this->_httpMethodType($args);
                break;
            default:
                // TODO: invalid method.
                break;
        }

        $this->_verb = $verb;

        // init and parse args only if it's set.
        if( !empty($args) ) {
            $this->_args = $this->initArgs($args);
        }

        $this->_request = $request;
    }

    ///--------------------------------------
    abstract protected function initArgs($args);

    ///--------------------------------------
    protected function doGetCollection() {
    }

    ///--------------------------------------
    protected function doGetElement() {
    }

    ///--------------------------------------
    protected function doPostCollection() {
    }

    ///--------------------------------------
    protected function doPostElement() {
    }

    ///--------------------------------------
    protected function doPutCollection() {
    }

    ///--------------------------------------
    protected function doPutElement() {
    }

    ///--------------------------------------
    protected function doDeleteCollection() {
    }

    ///--------------------------------------
    protected function doDeleteElement() {
    }

    ///--------------------------------------
    public static final function explodeURI($uri) {
        $args = explode('/', trim($uri, '/'));
        $data = [];

        // extract the data.
        $data['endpoint']  = array_shift($args);
        $data['classname'] = strtoupper($data['endpoint'][0]) . substr($data['endpoint'], 1, -1);  // upper case the first char and remove 's' char.
        $data['verb']      = (array_key_exists(0, $args) && !is_numeric($args[0]) ? array_shift($args) : '');
        $data['args']      = $args;

        return $data;
    }

    ///--------------------------------------
    public function processRequest() {
        // call the appropriet http method.
        $this->{$this->_httpMethod}();
    }

    ///--------------------------------------
    public function getResponse() {
        return $this->_response;
    }

    ///-------------------------------------
    public function getJsonResponse() {
        ini_set('display_errors', 0);                     // set it to off to disable show PHP erros
        header('Content-type: application/json');       // Specify The Content Type
        return json_encode((array)$this->_response);
    }
}
