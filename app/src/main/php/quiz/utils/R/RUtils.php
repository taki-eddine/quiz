<?php

class RUtils {
    public static function export($beans) {
        $array = [];

        foreach($beans as $bean)
            $array[] = $bean->export();
        return $array;
    }
}
