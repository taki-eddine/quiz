<?php
////////////////////////////////////////////////////////////////////////////////
include_once 'commun/Config.php';
include_once 'commun/RConfig.php';
////////////////////////////////////////////////////////////////////////////////

// give to `Tonic` the controllers to parse the @nnotations.
$api = new Tonic\Application([ 'load' => __DIR__ .'/controllers/*.php' ]);

// the acceptable `MimeTypes` and the response `MimeType`.
$request = new Tonic\Request([ 'accept'      => 'application/json',
                               'contentType' => 'application/json'
                             ]);

// get the controller wich will handle the requested resource.
$resource = $api->getResource($request);

// let the magic happen ;) .
$response = $resource->exec();
$response->output();
