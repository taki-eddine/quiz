<?php

//--------------------------------------
// set the include path directories.
set_include_path(get_include_path() . PATH_SEPARATOR . __DIR__);

//--------------------------------------
// create a class autoloader.
spl_autoload_register(function ($class_name) {
    $dirs = [ '/services/',
              '/utils/',
              '/utils/R/',
              '/controllers/'
            ];

    foreach( $dirs as $dir )
        if( @include_once($dir . $class_name .'.php') )
            return;
});
