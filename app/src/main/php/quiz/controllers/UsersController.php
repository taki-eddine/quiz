<?php

/**
 * @uri /users
 */
class UserCollection extends Tonic\Resource {
    /**
     * @method GET
     */
    public function index() {
        // TODO: use RedBeanPHP to get the object from the database.
        $users = [];

        $users[0] = new stdClass();
        $users[0]->name      = "john";
        $users[0]->last_name = "smith";

        $users[1] = new stdClass();
        $users[1]->name      = "agent";
        $users[1]->last_name = "47";
        return $users;
    }
}

/**
 * @uri /users/{id}
 */
class User extends Tonic\Resource {
    /**
     * @method GET
     */
    public function fetch($id) {
        // TODO: use RedBeanPHP to get the object from the database.
        $user = new stdClass();
        $user->name      = "john";
        $user->last_name = "smith";
        return $user;
     }
}
