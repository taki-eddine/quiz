<?php

/**
 * @uri /series/{serie_id}/qcms
 * @uri /series/{serie_id}/qcms/
 */
class SeriesQcms extends Tonic\Resource {
    /**
     * @method GET
     * @provides application/json
     */
    public function index($serie_id) {
        $serie = R::load('serie', $serie_id);
        return json_encode( RUtils::export($serie->sharedQcm) );
    }
}
