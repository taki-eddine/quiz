<?php

/**
 * @uri /qcms
 * @uri /qcms/
**/
class Qcms extends Tonic\Resource {
    /**
     * @method GET
     * @provides application/json
     */
    public function index() {
        $qcms = R::findAll("qcm");
        return json_encode( RUtils::export($qcms) );
    }
}

/**
 * @uri /qcms/{id}
 */
class Qcm extends Tonic\Resource {
    /**
     * @method GET
     * @provides application/json
     */
    public function show($id) {
        $qcm = R::load('qcm', $id);
        return json_encode( $qcm->export() );
    }
}
