<?php

/**
 * @uri /categories
 * @uri /categories/
 */
class Categories extends Tonic\Resource {
    /**
     * @method GET
     * @provides application/json
     */
    public function index() {
        $categories = R::findAll('category');
        return json_encode( RUtils::export($categories) );
    }
}

/**
 * @uri /categories/{id}
 */
class Category extends Tonic\Resource {
    /**
     * @method GET
     * @provides application/json
     */
    public function show($id) {
        $category = R::load('category', $id);
        return json_encode($category->export());
    }
}
