<?php

/**
 * @uri /categories/{category_id}/series
 * @uri /categories/{category_id}/series/
 */
class CategorySeries extends Tonic\Resource {
    /**
     * @method GET
     * @provides application/json
     */
    public function index($category_id) {
        $category = R::load('category', $category_id);
        return json_encode( RUtils::export($category->ownSerie) );
    }
}
