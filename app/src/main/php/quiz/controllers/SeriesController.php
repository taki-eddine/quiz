<?php

/**
 * @uri /series
 * @uri /series/
 */
class Series extends Tonic\Resource {
    /**
     * @method GET
     * @provides application/json
     */
    public function index() {
        $series = R::findAll('serie');
        return json_encode( RUtils::export($series) );
    }
}

/**
 * @uri /series/{id}
 */
class Serie extends Tonic\Resource {
    /**
     * @method GET
     * @provides application/json
     */
    public function show($id) {
        $serie = R::load('serie', $id);
        return json_encode( $serie->export() );
    }
}
