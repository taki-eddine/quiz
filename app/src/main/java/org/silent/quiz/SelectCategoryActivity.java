package org.silent.quiz;

////////////////////////////////////////////////////////////////////////////////
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.Bind;
import butterknife.ButterKnife;
import org.silent.quiz.model.Category;
import org.silent.quiz.remote.CategoriesService;
import org.silent.quiz.remote.WebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
////////////////////////////////////////////////////////////////////////////////

public class SelectCategoryActivity extends AppCompatActivity {
    @Bind(R.id.category_list) ListView category_list = null;
    private ArrayAdapter<Category>     list_adapter  = null;

    ///--------------------------------------
    private void loadCategories() {
        ///
        CategoriesService    service = WebService.createService(CategoriesService.class);
        Call<List<Category>> call    = service.listCategories();

        ///
        call.enqueue(new Callback<List<Category>>() {
            ///-------------------------------------
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                List<Category> categories = response.body();
                list_adapter.clear();
                list_adapter.addAll(categories);
                list_adapter.notifyDataSetChanged();
            }

            ///-------------------------------------
            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                Log.e("Quiz", t.getMessage());
            }
        });
    }


    /**
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_category);
        ButterKnife.bind(this);

        ///
        list_adapter = new ArrayAdapter<Category>(this, android.R.layout.simple_list_item_1, new LinkedList<Category>()) {
            ///-------------------------------------
            @Override
            public View getView(int position, View convert_view, ViewGroup parent) {
                // check if an existing view is being reused, otherwise inflate the view.
                if( convert_view == null )
                    convert_view = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, null);
                ((TextView)convert_view).setText(getItem(position).getName());
                return convert_view;
            }
        };
        category_list.setAdapter(list_adapter);

        ///
        category_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO: we have the ID of the Category, so Give it to the game Activity to load the series.
                Category category = (Category)parent.getItemAtPosition(position);
                Toast.makeText(SelectCategoryActivity.this, "Selected Category: id=" + category.getId() + ", name='" + category.getName() + "'", Toast.LENGTH_LONG).show();
            }
        });

        ///
        loadCategories();
    }
}
