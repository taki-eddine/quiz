package org.silent.quiz.remote;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WebService {
    public static final String BASE_URL = "http://10.0.3.2/quiz/";

    private static Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                                                             .addConverterFactory(GsonConverterFactory.create())
                                                             .build();

    ///--------------------------------------
    public static <Tp> Tp createService(Class<Tp> service_class) {
        return retrofit.create(service_class);
    }
}
