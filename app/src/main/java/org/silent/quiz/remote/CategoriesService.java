package org.silent.quiz.remote;

////////////////////////////////////////////////////////////////////////////////
import org.silent.quiz.model.Category;

import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;
////////////////////////////////////////////////////////////////////////////////

public interface CategoriesService {
    @GET("categories")
    Call<List<Category>> listCategories();
}
